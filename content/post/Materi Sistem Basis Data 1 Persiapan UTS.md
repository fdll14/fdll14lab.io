---
title: "Materi Sistem Basis Data 1 Persiapan UTS"
date: 2020-11-05T15:43:48+08:00
lastmod: 2020-11-05T15:43:48+08:00
draft: false
tags: ["Catatan","Sistem Basis Data 1"]
categories: ["Mata Kuliah"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Halo

Ini merupakan catatan pribadi saya untuk persiapan UTS.


##### Cara login oracle database

ketikan perintah `SQLPLUS`

untuk username isi `SYS AS SYSDBA`
untuk password isi `oracle`

jika berhasil maka akan muncul tulisan

```
Connected to:
Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production
```

##### Cara membuat user baru di oracle database

ketikan perintah `CREATE USER KELAS3D19090072 IDENTIFIED BY oracle;`

keterangan : 

nama username adalah `KELAS3D19090072`dan password adalah `oracle`.

##### Cara untuk terhubung / terkoneksi

ketikan perintah :  `GRANT CONNECT, RESOURCE TO KELAS3D19090072;`

Output jika berhasil
```
Grant succeeded.
```

##### Jenis jenis tipe data
`CHAR`, untuk menyimpan data karakter dengan panjang tetap.

`VARCHAR2`, untuk menyimpan data karakter dengan panjang dinamis.

`NUMBER`(precision, scale), untuk menyimpan nilai floating point (bilangan pecahan).

`DATE`, Format tanggal berupa DD/MM/YYYY (Tanggal, Bulan, dan Tahun saja).

Untuk jenis tipe data yang lainnya
[Klik disini](https://nursalimbox.blogspot.com/2016/01/pengenalan-tipe-data-pada-oracle-plsql.html)

##### Cara membuat table di oracle database

cara pengetikannya adalah seperti berikut : `CREATE TABLE namatable ( isi table );`

contoh :

CREATE TABLE admin (
id_admin CHAR(7),
nama VARCHAR2(30),
gender CHAR(1),
no_hp VARCHAR2(14),
email VARCHAR2(30),
tanggal_lahir DATE
);

Untuk menampilkan table yang telah kita buat dengan cara mengetikan perintah DESCRIBE nama table seperti berikut : `DESCRIBE admin `

![gambar 1](https://imgur.com/bl86ndc.png)

##### Cara menghapus sebuah table

salah memberikan nama table dan ingin menghapusnya gunakan perintah `DROP TABLE namatable` contoh : `DROP TABLE DETAIL_BELI`

![](https://imgur.com/7YWVALR.png)

##### cara melihat isi table 

bagaimana cara mengecek hasil input yang sudah kita masukan ke dalam table ? 
caranya dengan mengetikan perintah `SELECT * FROM namatable` 

contoh menampilkan isi table admin : `SELECT * FROM admin`

![admin](https://imgur.com/mFzeeTL.png)

##### Cara memasukan isi kedalam table di oracle database

untuk cara memasukannya adalah dengan mengetikan perintah : `INSERT INTO`

contoh :

```
INSERT INTO admin (id_admin, nama, gender, no_hp, email, tanggal_lahir)
VALUES ('2010A01', 'Karin', 'P', '08123246360', 'karin@mail.com', '17-06-1991');
```

Cara simple 

```
INSERT INTO admin VALUES ('2010A01', 'Karin', 'P', '08123246360', 'karin@mail.com', '17-06-1991');
```

##### Mengalami error saat memasukan tanggal ?

```
ERROR at line 2:
ORA-01843: not a valid month
```

ini dikarenaka tipe penulisan tanggal pada oracle adalah seperti berikut `09-Nov-2020` dimana di bagian bulan bukan angka untuk mengtasinya dengan mengetikan perintah dibawah :

```
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';
SELECT TO_DATE('09-11-2000') FROM dual;
```
![ntah](https://imgur.com/OaNMcKu.png)


##### jangan lupakan COMMIT!!!

Perintah COMMIT digunakan untuk menyimpan hasil inputan yang sudah kita lakukan ke dalam database agar disimpan secara permanen. Dan apa yang terjadi jika kita lupa melakukan COMMIT ? bisa dipastikan data yang sudah kita inputkan akan hilang.

untuk perintahnya adalah : `COMMIT;`

##### ROLLBACK is back !!!

Perintah rollback adalah cara untuk mengembalikan semua perubahan yang terjadi sebelum kita melakukan COMMIT

untuk perintahnya adalah : `ROLLBACK;`



#### Perintah perintah Constraint

##### Cara membuat constraint check

untuk membuat check email contohnya seperti dibawah 

```
ALTER TABLE admin ADD CONSTRAINT adm_eml_ck CHECK (email LIKE '%@%.%');
```

untuk membuat check gender contohnya seperti dibawah :

```
ALTER TABLE admin ADD CONSTRAINT adm_gnd_ck CHECk(gender IN('P','L'));
```

untuk membuat check no hp contohnya seperti dibawah :

```
ALTER TABLE admin ADD CONSTRAINT adm_no_hp_ck CHECK (no_hp LIKE '08%');
```
![ss](https://imgur.com/DIFdQ1t.png)

##### Cara membuat primary key 

untuk membuat primarykey contohnya seperti dibawah :

```
ALTER TABLE admin ADD CONSTRAINT adm_pk PRIMARY KEY(id_admin);
```

contoh lain 

```
ALTER TABLE supplier ADD CONSTRAINT spl_pk PRIMARY KEY(id_supplier);
```

##### Cara membuat Unique

untuk membuat unique no hp contohnya seperti dibawah :

```
ALTER TABLE admin ADD CONSTRAINT adm_no_hp_uq UNIQUE(no_hp);
```

untuk membuat unique nama contohnya seperti dibawah :

```
ALTER TABLE admin ADD CONSTRAINT adm_nama_uq UNIQUE(nama);
```

untuk membuat unique email contohnya seperti dibawah :

```
ALTER TABLE admin ADD CONSTRAINT adm_eml_uq UNIQUE(email);
```

![ss](https://imgur.com/2IELbKF.png)
##### Cara check apa saja constraint yang sudah ada pada database kita

contoh menampilan constraint apa saja yg sudah saya buat pada table `ADMIN`

```
SELECT constraint_name, constraint_type, table_name FROM all_constraints
WHERE table_name = 'ADMIN'
ORDER BY table_name, constraint_type;
```
ket : `c` adalah `CHECK` , `p` adalah `primary key` , dan `u` adalah `unique`

![](https://imgur.com/nLawdsF.png)

untuk mengecek constraint pada beberapa table adalah dengan menambahkan  `OR` contoh perintahnya adalah sebagai berikut
```
SELECT constraint_name, constraint_type, table_name FROM all_constraints
WHERE table_name = 'ADMIN' OR table_name = 'SUPPLIER' OR table_name = 'PEMBELIAN' OR table_name = 'DETIL_BELI'
ORDER BY table_name, constraint_type;
```
kita tinggal ganti aja kata `ADMIN` , `SUPPLIER`, `PEMBELIAN`, dan `DETIL_BELI` dengan table yang ingin kita inginkan.

![](https://imgur.com/b2pUPYK.png)

##### Sumber : 

[Mengatasi error month not a valid](https://www.codeproject.com/Tips/997250/How-to-Resolve-the-Not-a-Valid-Month-Error-with-Or)