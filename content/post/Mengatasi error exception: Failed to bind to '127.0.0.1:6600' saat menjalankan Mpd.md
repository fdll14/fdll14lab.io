---
title: "Mengatasi error exception: Failed to bind to '127.0.0.1:6600' saat menjalankan Mpd"
date: 2020-06-22T06:43:48+08:00
lastmod: 2020-06-22T06:43:48+08:00
draft: false
tags: ["Linux experience","tutorial"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---


#### Hello

Kali saya mau berbagi pengalaman saya saat menggunakan Mpd di distro MX linux namun saat mau menjalankan mpdnya mengalami error seperti ini : 

```sh
Jun 22 05:28 : exception: Failed to bind to '127.0.0.1:6600'
Jun 22 05:28 : exception: nested: Failed to bind socket: Address already in use
```

Bagi teman teman yang belum tau apa itu MPD , **MPD atau Music Player Daemon** ini adalah sebuah server daemon yang gunanya untuk memutar music dan NCMPCPP adalah client atau sebuah aplikasi yang digunakan untuk manajemen file musik dan sebagainya, dan kelebihan dari tool ini dapat mengkonfigurasi nya sesuai dengan yang kita inginkan seperti tampilan menu, daftar lagu, visualizer dan sebagainya.

Kenapa menggunakan mpd kalo pemutar musik yang lain ada ? alasannya ya karna pengin aja hehe


##### Cara penyelesaian

Untuk cara mengatasi error tersebut kalian bisa mengetikan perintah dibawah pada terminal :

```sh
sudo killall mpd
mpd &
```

Sumber : [Reddit](https://www.reddit.com/r/linux4noobs/comments/8xu6b6/mpd_help/)