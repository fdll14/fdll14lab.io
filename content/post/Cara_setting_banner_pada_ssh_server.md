---
title: "Cara setting banner pada ssh server"
date: 2020-06-24T15:43:48+08:00
lastmod: 2020-06-24T15:43:48+08:00
draft: false
tags: ["Linux experience","tutorial"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---


#### Hello

Pada artikel kali ini saya mau membahas tentang bagaimana sih caranya menambahkan banner pada proses login ke dalam ssh server. bagi temen temen yang bertanya tanya gunanya buat apa sih banner ini ?

banner pada ssh server sering digunakan sebagai alat atau media informasi entah itu untuk si administator, atau pun orang lain. Tapi banner yang sering saya temui biasanya digunakan untuk media informasi tentang pemberitahuan untuk larangan penggunaan seperti tindakan ddos attack atau hal hal yang melanggar hukum. Itu sih yang sering saya temui dulu saat menggunakan ssh dari penyedia ssh gratisan seperti di [fastssh.com](fastssh.com) , [sshdropbear.net](sshdropbear.net) maupun penyedia ssh gratis lainnya. Btw saya dulu make ssh buat gretongan maklum saya penganut filosofi kere hore hehe..


#### Tutorial

Ada 2 Banner yang bisa ditampilkan di ssh server yaitu saat login dan juga pada saat setelah login

1. **Banner saat login**
Untuk tampilan banner saat login kira kira seperti screenshot dibawah :
![Screenshoot.](https://u.cubeupload.com/fdll14/Cheese200624180027.png)
2. **Banner setelah login**
![Screenshoot](http://u.cubeupload.com/fdll14/Cheese200624181251.png)

##### Banner login

Untuk tutorialnya sebenarnya cukup simpel pertama untuk membuat tampilan banner saat login kita bisa membuat file di dalam dir ssh server yang berada di dir **/etc/ssh/** dengan cara mengetikan perintah :


>sudo gedit /etc/ssh/banner.txt


lalu isi dengan txt ini

```
################################################

#   Selamat Datang di server ssh fadil.my.id   #

################################################ 
```
kalo sudah kita edit file **sshd_config** dengan mengetikan perintah


>sudo nano /etc/ssh/sshd_config

lalu tambahan text dibawah

>Banner /etc/ssh/banner.txt

![Screenshot](http://u.cubeupload.com/fdll14/Cheese200624182423.png)

setelah selesai kita tinggal restart ssh-servernya dengan mengetikan perintah :


>sudo /etc/init.d/ssh restart


###### Banner setelah login

Untuk membuat banner setelah login kita hanya tinggal mengedit file motd yang berada pada dir **etc** 


>sudo gedit /etc/motd

lalu isi dengan txt ini

```
################################################

#   Selamat anda sudah berhasil login	       #
#        di server ssh fadil.my.id   	       #

################################################ 
```

kalo sudah kita edit file motdnya langsung restart service sshnya lagi

>sudo /etc/init.d/ssh restart


jika ada yang kalian bingungkan kalian bisa komentar dikolom komentar.