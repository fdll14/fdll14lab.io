---
title: "Pengalaman Menggunakan Git"
date: 2020-06-18T21:27:00+08:00
lastmod: 2020-06-18T21:27:00+08:00
draft: false
tags: ["git","linux experience"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

### Apa itu git ?

Git adalah salah satu sistem pengontrol versi (Version Control System) pada proyek perangkat lunak yang diciptakan oleh Linus Torvalds.

Pengontrol versi bertugas mencatat setiap perubahan pada file proyek yang dikerjakan oleh banyak orang maupun sendiri.

Git dikenal juga dengan distributed revision control (VCS terdistribusi), artinya penyimpanan database Git tidak hanya berada dalam satu tempat saja.

ya kira kira seperti itu penjelasan tentang git namun disini saya bukan mau menjelaskan tentang kegunaan git tapi pengalaman pait belajar menggunakan git untuk push codingan ke layanan seperti gitlab maupun github

#### error rejected

Pengalaman yang selalu saya dapatkan saat pertama kali ingin push repository dari lokal menggunakan git adalah error 

```sh
! [rejected]        master -> master (fetch first)
```

setelah berselancar di google buka situs tutup situs berkali kali akhirnya saya temukan juga solusinya ya walau ini sebenernya bukan pemecah masalahnya saya menggunakan perintah 

```sh
git push origin master --force
```

bagi sebagai pengguna git setelah melakukan perintah ini masalah mereka selesai tapi tidak dengan saya, karna setelah menjalankan perintah diatas saya mendapati pesan error lagi seperti ini 

#### error remote rejected

```sh
! [remote rejected] master -> master (pre-receive hook declined)
```

setelah mencoba gooling lagi saya jadi tau penyebabnya yaitu di pengaturan repository saya sendiri yang berada pada gitlab.

untuk mengatasinya bisa dengan cara mengubah pengaturan di `setting > repository > Protected branch ` lalu matikan protect