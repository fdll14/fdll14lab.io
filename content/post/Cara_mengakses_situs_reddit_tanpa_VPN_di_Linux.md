---
title: "Cara Mengakses Situs Reddit tanpa VPN di Linux"
date: 2020-04-02T15:43:48+08:00
lastmod: 2020-04-02T15:43:48+08:00
draft: false
tags: ["Linux experience","tutorial"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Halo

Pada artikel kali ini saya mau memeberikan tutorial tentang **Cara Mengakses Situs Reddit tanpa VPN di Linux** karena di indonesia sendiri situs Reddit di blokir dikarenakan katanya di Reddit ada konten dewasanya. Kalo saya sendiri mengujungi situs reddit untuk mencari tutorial untuk menyelesaikan problem yang saya temui saat menggunakan linux. Sebenernya banyak cara untuk mengakses situs ini mulai dari menggunakan VPN , menggunakan DNScrypt, menggunakan Web Proxy, dll.
Namun saya lebih suka mengedit file hosts dan menambahkan host atau IP asli dari Reddit ke file host saya.

#### Tutorial

Untuk tutorialnya juga cukup simple dan mudah kita hanya tinggal mengedit file hosts yang berada pada directory etc dengan menggunakan perintah



>sudo nano /etc/hosts

nb : kalian bisa mengganti nano dengan text     editor yang kalian inginkan

kalo sudah tambahkan host berikut :

```sh
## Reddit
151.101.129.140 redd.it
54.172.97.229 redd.it
151.101.129.140 i.redd.it
151.101.129.140 reddit.com
151.101.129.140 4x.reddit.com
151.101.129.140 api.reddit.com
151.101.129.140 about.reddit.com
151.101.129.140 blog.reddit.com
151.101.129.140 bt.reddit.com
151.101.129.140 e.reddit.com
151.101.129.140 en.reddit.com
151.101.129.140 en-us.reddit.com
151.101.129.140 forum.reddit.com
151.101.129.140 gateway.reddit.com
151.101.129.140 help.reddit.com
151.101.129.140 i.reddit.com
151.101.129.140 m.reddit.com
151.101.85.140 m.reddit.com
151.101.129.140 nm.reddit.com
151.101.129.140 np.reddit.com
151.101.129.140 nsfw.reddit.com
151.101.129.140 oauth.reddit.com
151.101.129.140 out.reddit.com
54.174.14.76 out.reddit.com
151.101.129.140 pay.reddit.com
151.101.129.140 ssl.reddit.com
151.101.129.140 test.reddit.com
151.101.129.140 ww.reddit.com
151.101.129.140 www.reddit.com
192.0.79.33 redditblog.com
192.0.79.33 www.redditblog.com
151.101.129.140 redditgifts.com
151.101.129.140 www.redditgifts.com
151.101.129.140 redditmedia.com
151.101.129.140 events.redditmedia.com
151.101.129.140 g.redditmedia.com
151.101.129.140 i.redditmedia.com
151.101.129.140 pixel.redditmedia.com
151.101.129.140 stats.redditmedia.com
151.101.129.140 www.redditmedia.com
151.101.129.140 a.thumbs.redditmedia.com
151.101.129.140 b.thumbs.redditmedia.com
151.101.129.140 redditstatic.com
151.101.129.140 www.redditstatic.com
151.101.129.140 reddituploads.com
151.101.129.140 i.reddituploads.com
151.101.129.140 www.reddituploads.com
```

Jika sudah save. Lalu coba akses Reddit. Seharusnya sekarang sudah bisa diakses tanpa menggunakan VPN.


#### Sumber :  
[LinuxSec - Cara Mengakses Situs Reddit tanpa VPN di Linux ](https://www.linuxsec.org/2019/08/mengakses-situs-reddit-tanpa-vpn.html)
