---
title: "Materi Sistem Basis Data 1 Pertemuan Pertama"
date: 2020-10-11T15:43:48+08:00
lastmod: 2020-10-11T15:43:48+08:00
draft: false
tags: ["Catatan","Sistem Basis Data 1"]
categories: ["Mata Kuliah"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Halo

Ini merupakan catatan pribadi saya tentang mata kuliah sistem basis data


##### Cara login oracle database

ketikan perintah `SQLPLUS`

untuk username isi `SYS AS SYSDBA`
untuk password isi `oracle`

jika berhasil maka akan muncul tulisan

```
Connected to:
Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production
```

##### Cara membuat user baru di oracle database

ketikan perintah `CREATE USER fadil IDENTIFIED BY fadil123;`

keterangan : 

untuk kata `fadil` di dalam baris perintah diatas digunakan untuk menjadi nama user kita sedangkan kata `fadil123` adalah nama password untuk user baru yang kita bikin tadi.

##### Cara untuk terhubung / terkoneksi

note : cara ini digunakan jikalau kita baru membuat user baru dan masih login menggunakan `SYS AS SYSDBA`.

ketikan perintah : `SHOW USER`

untuk mengecek user apa yang sedang kita gunakan. Jika muncul tulisan seperti dibawah bisa dipastikan kita login menggunakan `SYS AS SYSDBA`.

```
SQL> SHOW USER
USER is "SYS"
```
ketikan perintah :  `GRANT CONNECT, RESOURCE TO fadil;`

perintah diatas digunakan untuk terkoneksi dengan akun baru yang kita bikin dimana kata `fadil` adalah nama user yang kita bikin sebelumnya. Jika berhasil maka akan ada output seperti dibawah.

```
Grant succeeded.
```

##### Cara membuat table di oracle database

ketikan perintah : `CREATE TABLE namatable ( isi table );`

contoh saya membuat table keluarga dengan isi nomor, nama, jenis_kelamin, tempat_lahir, tanggal_lahir, status, nik, no_hp, pekerjaan dan instagram.

```
CREATE TABLE keluarga ( nomor NUMBER, nama VARCHAR2(23), jenis_kelamin CHAR(9), tempat_lahir VARCHAR(5), tanggal_lahir DATE, status VARCHAR2(15), nik CHAR(16), no_hp VARCHAR2(14), pekerjaan VARCHAR2(20), instagram VARCHAR2(10) );
```
Untuk menampilkan table yang telah kita buat dengan cara mengetikan perintah : `DESCRIBE keluarga`

note : kata `keluarga` dalam perintah tersebut adalah nama table yang ingin saya lihat.

SQL> describe keluarga
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 NOMOR                                              NUMBER
 NAMA                                               VARCHAR2(23)
 JENIS_KELAMIN                                      CHAR(9)
 TEMPAT_LAHIR                                       VARCHAR2(5)
 TANGGAL_LAHIR                                      DATE
 STATUS                                             VARCHAR2(15)
 NIK                                                CHAR(16)
 NO_HP                                              VARCHAR2(14)
 PEKERJAAN                                          VARCHAR2(20)
 INSTAGRAM                                          VARCHAR2(10)


##### Cara memasukan isi kedalam table di oracle database

untuk cara memasukannya adalah dengan mengetikan perintah : `INSERT INTO`

contoh :

```
INSERT INTO keluarga (nomor, nama, jenis_kelamin, tempat_lahir, tanggal_lahir, status, nik, no_hp, pekerjaan, instagram)
  1  VALUES (3, 'Muhamad Shuro Fadhillah', 'Laki laki', 'Tegal', '09-NOV-2000', 'Anak' , '3328xxxxxxxxxxx', '0895422836123', 'Pelajar/Mahasiswa','fdll_14');
```



##### Apa itu COMMIT ???

Perintah COMMIT digunakan untuk menyimpan hasil inputan yang sudah kita lakukan ke dalam database agar disimpan secara permanen. Dan apa yang terjadi jika kita lupa melakukan COMMIT ? bisa dipastikan data yang sudah kita inputkan akan hilang.

untuk perintahnya adalah : `COMMIT;`




##### Sumber : 

Ini merupakan hasil catatan pribadi saya sendiri dari hasil belajar saya di kampus.