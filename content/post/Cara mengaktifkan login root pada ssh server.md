---
title: "Cara mengaktifkan login root pada ssh server"
date: 2020-03-30T15:43:48+08:00
lastmod: 2020-03-30T15:43:48+08:00
draft: false
tags: ["Linux experience","tutorial"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---


#### Hello

Untuk catatan kali ini adalah masalah yang saya alami saat ingin remote leptop saya yang menggunakan distro debian menggunakan ssh, sebenernya saya iseng aja ingin meremot menggunakan aplikasi ConnectBot semacam aplikasi yang mirip dengan Putty, namun tersedia dalam versi mobile. Prosesnya sama, memasukkan ke dalam kolom address : user@namaserver atau user@ipaddress. Dengan melengkapi password yang benar maka server akan terhubung.

Singkat cerita saya mengalami error saat menggunakan username root untuk login ke distro debian saya padahal password sudah benar.

Dari situ saya googling untuk menyelesaikan problem yang saya alami itu dan akhirnya ketemu deh penyebab masalahnya.
yang disebabkan configurasi ssh saya tidak mengijinkan akses root menggunakan ssh.


#### Cara penyelesaian

Untuk cara memberikan akses menggunakan root pada ssh cukup mudah kita hanya harus mengedit file config ssh kita yang berada pada directory `/etc/ssh/sshd_config` dengan cara mengetikan perintah dibawah pada terminal :

```
sudo nano /etc/ssh/sshd_config
```

lalu cari tulisan `Authentication` lalu ubah menjadi seperti dibawah ini :

```sh
Authentication
LoginGraceTime 120 
PermitRootLogin Yes 
StrictModes yes
```

nb : jangn lupa hapus # 

setelah selesai Restart ssh servicenya dengan perintah dibawah pada terminal :


>/etc/init.d/ssh restart


Setelah selesai sekarang kita bisa login ssh menggunakan root.

jika ada yang kalian bingungkan kalian bisa komentar dibawah atau menghubungi saya melalui [Facebook](https://web.facebook.com/fdll14)