---
title: "Cara mengatasi error Systemd-backlight pada ArchLabs"
date: 2020-04-01T15:43:48+08:00
lastmod: 2020-01-02T15:43:48+08:00
draft: false
tags: ["Linux experience","tutorial"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---


#### Hello

Saat saya menyalakan leptop saya untuk boot ke archlabs saya melihat ada yang error di proses bootnya yaitu error pada backlightnya sebenernya gak ada masalah saat saya menggunakannya walau ada error tersebut namun karna gabut saya memutuskan untuk mencara penyebabnya dari situ saya tau ternyata notif error tersebut disebabkan karna **screen backlight brightness tidak misa melakukan start load/save**. dari situ saya memutuskan untuk memperbaiki masalahnya setelah melakukan googling ketemu cara penyelesainya dari situs [bbs.archlinux.org](bbs.archlinux.org) dan ini cara mengatasinya : 


#### Solved

pertama cek dulu

```sh
systemctl status systemd-backlight@backlight:acpi_video0.service
``` 

jika muncul error seperti dibawah berarti kalian mengalami error yang sama seperti yang saya alami:

```sh
journalctl -b _PID=341
-- Logs begin at sab 2016-04-23 03:36:55 CEST, end at ven 2016-04-29 02:01:02 CEST. --
apr 29 01:38:49 alba systemd-backlight[341]: Failed to get backlight or LED device 'backlight:acpi_video0': No such device
```

nah untuk penyelesainya kalian buka file config grub-menu menggunakan terminal

>sudo nano /etc/default/grub

lalu tambahkan perintah dibawah :

>GRUB_CMDLINE_LINUX_DEFAULT=" aspi_backlight=vendor"


setelah itu lakukan update grub dengan menggunakan perintah dibawah :

> grub-update


untuk pengguna arch gunakan perintah 

>sudo grub-mkconfig -o /boot/grub/grub.cfg

setelah selesai lakukan perintah reboot untuk mengeceknya

