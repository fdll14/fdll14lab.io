---
title: "Materi Sistem Basis Data 1 Pertemuan Kedua"
date: 2020-10-11T15:43:48+08:00
lastmod: 2020-10-11T15:43:48+08:00
draft: false
tags: ["Catatan","Sistem Basis Data 1"]
categories: ["Mata Kuliah"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Halo

Ini merupakan catatan pribadi saya tentang mata kuliah sistem basis data


##### Cara merubah ukuran garis pada isi table

ketikan perintah : `SET LINESIZE 250`

note : untuk nilai `250` bisa diganti sesuai selera.

##### Cara menampilkan isi dari suatu table

ketikan perintah : `SELECT * FROM keluarga`

note : baris perintah diatas digunakan untuk menampilkan isi dari table keluarga

##### Cara menghapus suatu table pada oracle database

ketikan perintah : `DELETE keluarga;` 

note : baris perintah diatas digunakan untuk menghapus isi table keluarga

##### Apa itu ROLLBACK

perintah rollback adalah cara untuk mengembalikan semua perubahan yang terjadi sebelum di COMMIT

ketikan perintah : `ROLLBACK`

##### Cara menghapus layar atau clear screen

ketikan perintah  : `CLEAR SCREEN`


##### Cara mengecek format tanggal pada oracle database

ketikan perintah : `SELECT CURRENT_DATE FROM dual;`

##### Cara menambahkan primary key pada suatu kolom di dalam table

ketikan perintah : `ALTER TABLE keluarga ADD CONSTRAINT no_pk PRIMARY KEY(nomor);`

Note :kata `nomor` adalah nama kolom yang akan ditambahkan primary key

##### Cara menambahkan unique pada suatu kolom di dalam table

ketikan perintah : `ALTER TABLE keluarga ADD CONSTRAINT nama_unique UNIQUE(nama);` 

Note : kata `nama` adalah nama kolom yang akan ditambahkan unique

##### Cara mengecek primary key dan unique pada suatu table

ketikan perintah : `SELECT constraint_name, constraint_type FROM all_constraints WHERE table_name = 'KELUARGA';`

note : kata `KELUARGA` pada baris perintah diatas adalah nama table.

##### Cara menganti nama suatu table pada oracle database

ketikan perintah : `ALTER TABLE keluarga RENAME TO keluarga_saya;`

note : pada baris perintah diatas digunakan untuk merubah nama table `keluarga` menjadi nama table `keluarga_saya`.

##### Cara menganti nama kolom pada suatu table 

ketikan perintah : `ALTER TABLE keluarga RENAME COLUMN ig TO instagram;` 

note : baris perintah diatas digunakan untuk merubah nama kolom `ig` menjadi `instagram` pada table `keluarga`.

##### Cara merubah nilai value pada kolom suatu table

ketikan perintah : `ALTER TABLE keluarga MODIFY tempat_lahir VARCHAR2(10);` 

note : baris perintah diatas digunakan untuk merubah jumlah maksimal value pada suatu kolom.

##### Cara menghapus isi table pada suatu baris

ketikan perintah : `DELETE keluarga WHERE nomor = 4;`

note : baris perintah diatas digunakan untuk menghapus isi table no 4 pada table keluarga.


##### Sumber : 

Ini merupakan hasil catatan pribadi saya sendiri dari hasil belajar saya di kampus.