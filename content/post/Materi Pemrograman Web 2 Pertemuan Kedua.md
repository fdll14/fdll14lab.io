---
title: "Materi Pemrograman Web 2 Pertemuan Kedua"
date: 2021-03-19T15:43:48+08:00
lastmod: 2021-03-19T15:43:48+08:00
draft: false
tags: ["Catatan","Pemrograman Web 2"]
categories: ["Mata Kuliah"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
# contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Halo
Ini merupakan catatan pribadi saya tentang mata kuliah Pemrograman Web. Pada materi minggu kedua ini diajarkan tentang membuat server lokal menggunakan apache dan mysql

nb : saya ansumsikan kalian sudah menginstall package package yang dibutuhkan seperti apache2 mysql php dan phpmyadmin

##### Setting apache2
Masuk folder configurasi apache2

```
cd /etc/apache2/sites-available/
```

lalu copy file 000-default.conf menjadi myprojects.conf

```
sudo cp 000-default.conf myprojects.conf
```

Jika sudah edit file myprojects.conf

```
sudo nano myprojects.conf
```

ubah `*` pada baris paling atas menjadi ip 127.0.0.10
lalu tambahkan baris dibawah

```
ServerName myprojects.local
ServerAlias myprojects.local

ServerAdmin webmaster@myprojects.local
DocumentRoot /var/www/myprojects
```
jika sudah tekan ctrl x lalu simpan dengan mengetik y

Lanjut buat folder dengan nama myprojects pada directory /var/www/

```
sudo mkdir -p /var/www/myprojects
```

ubah permissionnya

```
sudo chmod 777 -R /var/www/myprojects
```

buat satu file bernama index.html pada folder myproject 

```
nano /var/www/myproject/index.html
```

Isi file index.html dengan code dibawah
```
<html>
    <head>
        <title> myprojects </title>
    </head>
    <body>
        <center><h1> myprojects - website </h1></center>
    </body>
</html>
```
jika sudah tekan ctrl x lalu simpan dengan mengetik y

##### Setting hosts

Buka file hosts
```
sudo nano /etc/hosts
```
lalu tambahkan ip 127.0.0.10 dengan domain myproject.local

```
127.0.0.1   localhost
127.0.0.2   localheart.id
127.0.0.1   Shura
127.0.0.10  myprojects.local
```

jika sudah tekan ctrl x lalu simpan dengan mengetik y


##### restart apache2 dan enable konfigurasi
perintah restart apache2
```
sudo /etc/init.d/apache2 restart
```
nb : Pastikan apache2 sudah terestart


perintah enbale configurasi
```
sudo a2ensite myprojects.conf
```

Jika sudah silahkan check hasilnya dengan mengakses domain myprojects.local

##### Hasil

![hasil](https://imgur.com/eoPdAJs.png)

##### Sumber : 
Ini merupakan hasil catatan pribadi saya sendiri dari hasil belajar saya di kampus.

jangan lupa join group diskus kami [disini](https://t.me/buayacoding)
