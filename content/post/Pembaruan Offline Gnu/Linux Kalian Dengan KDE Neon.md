---
title: "Pembaruan Offline Gnu/Linux Kalian Dengan KDE Neon"
date: 2021-04-11T15:43:48+08:00
lastmod: 2021-04-11T15:43:48+08:00
draft: false
tags: ["News","Gnu/Linux"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---


#### Hello

Untuk catatan kali ini saya mau membahas tentang fitur terbaru dari KDE Project yaitu Pembaruan Offline. Pembaruan Offline disini maksudnya kita bisa melakukan pembaruan sistem Gnu/Linux kita saat booting dilakukan layaknya seperti pembaruan offline yang ada di sistem operasi Windows. 

Jadi kita hanya melakukan pengunduhan dan penginstallanya akan dilakukan saat booting berikutnya. oh ya pembaruan offline ini hanya tersedia ketika memperbarui instalasi kamu menggunakan KDE Manager Plasma Discover.

Dilansir dari kanal blog KDE neon fitur pembaruan offline ini sudah bisa dirasakan untuk semua edisi distro ini mulai dari tanggal 1 April kemarin, yang sebelumnya hanya tersedia untuk versi Unstable Edition.

![Foto pembaruan offline KDE](https://blog.neon.kde.org/wp-content/uploads/2021/03/Screenshot_neonUEFI_2021-03-01_151815-1024x684.png)


Apa saja keuntungan dengan adanya fitur pembaruan offline ini ?

1. Pertama alur kerja kita tidak akan terganggu saat proses pembaruan berlangsung pasalnya terkadang kita tidak bisa melakukan beberapa perintah diterminal saat kita melakukan pembaruan.
2. Kedua tidak jarang dalam beberapa kasus yang jarang terjadi, sistem mungkin menjadi tidak dapat diandalkan selama pembaruan, jadi kamu mungkin ingin mempertimbangkan untuk menggunakan pembaruan offline jika ini  pernah terjadi pada kamu di masa lalu.
3. Selain itu, pembaruan offline dapat menarik lebih banyak pengguna Windows yang ingin beralih ke Linux.
	
Di Linux, semua orang tahu bahwa menerapkan pembaruan tidak memerlukan reboot, kecuali untuk pembaruan kernel, dan itu masih ada di KDE neon jika Anda memperbarui instalasi kamu dari baris perintah atau menggunakan manajer paket grafis yang berbeda, seperti Synaptic Package Manager.


