---
title: "Cara membuat blog menggunakan hugo"
date: 2020-06-19T15:43:48+08:00
lastmod: 2020-06-19T15:43:48+08:00
draft: false
tags: ["Linux experience","tutorial"]
categories: ["linux experience"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Apa itu hugo ?

Pada artikel kali ini saya mau berbagi pengalaman saya mencoba menggunakan hugo sekaligus tutorial membuat blog mengguna hugo sebelumnya bagi teman teman yang belum tau apa itu [hugo](https://gohugo.io/) , hugo itu sederhannya adalah sebuah static site/website generator.

Static site generator mengijinkan kamu memegang kendali penuh atas konten kamu dan output nya akan berupa versi statis dari website kamu.

Yang dimaksud versi statis adalah, web yang hanya berisikan html css dan javascript.

Ada banyak pilihan static site generator, seperti [Jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io), [Hexo](https://hexo.io/), [Gatsbyjs](https://www.gatsbyjs.org/), [Nuxtjs](https://nuxtjs.org/), [Mkdocs](http://www.mkdocs.org/) dan masih banyak yang lainnya. namun disini saya hanya akan membahas hugo ya sesuai judul lah wkwk

#### Install Hugo

Untuk menginstall hugo cukup mudah kita tinggal mengetikan perintah dibawah pada terminal

>sudo apt-get install hugo

dan hugo akan terinstall otomatis di GNU/Linux kamu. Selanjutnya ketikkan perintah berikut untuk memastikan hugo benar-benar terinstall.


>hugo version


untuk hasil outputnya seperti ini


> Hugo Static Site Generator v0.72.0-8A7EF3CF linux/amd64 BuildDate: 2020-05-31T12:07:45Z


Untuk platform lain, silakan cari tahu bagaimana cara menginstallnya di halaman dokumentasi resmi Hugo disini.

#### Membuat blog

Setelah kita berhasil menginstall hugonya langkah selanjutnya adalah membuat sebuah situs baru dengan perintah berikut di terminal :
>hugo new site blogbaru


untuk hasil outputnya seperti ini :


>Congratulations! Your new Hugo site is created in /home/fadil/Documents/Deploy/blogbaru.
>
>Just a few more steps and you're ready to go:
>
> 1. Download a theme into the same-named folder.
>   Choose a theme from https://themes.gohugo.io/ or
>   create your own with the "hugo new theme <THEMENAME>" command.
>2. Perhaps you want to add some content. You can add single files
>   with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
>3. Start the built-in live server via "hugo server".
>
>Visit https://gohugo.io/ for quickstart guide and full documentation.


perintah diatas akan membuat sebuah situs hugo baru dengan folder bernama `blogbaru`

dengan struktur folder seperti berikut ini.

```
├── archetypes
│   └── default.md
├── config.toml
├── content
├── data
├── layouts
├── static
└── themes
```
 untuk mengecek struktur foldernya kalian bisa menggunakan perintah tree di dalam folder blogbaru sebelumnya jangan lupa install dulu packagenya

 
 > sudo apt-get install tree

 untuk mengeceknya tinggal ketik `tree` pada terminal

 untuk screenshotnya seperti ini

 ![Screenshot perintah tree.](/static/image/2020/juni/tree.png)


#### Pasang tema

Selanjutnya kita akan menambahkan tema. Tema adalah bagian penting di dalam Situs Hugo karena ini akan menunjukkan bagaimana konten kamu ditampilkan.

Pertama pastikan kita berada pada folder yang berisi situs hugo yang baru kita buat tadi.

lalu kita akan menginstall themes bernama `jane` atau nantinya kamu bisa memilih themes apa yang kamu mau di [directory themes resmi dari Hugo](https://themes.gohugo.io/)


 >git clone https://github.com/xianmin/hugo-theme-jane.git --depth=1 themes/jane


Salin konten situs contoh:

>cp -r themes/jane/exampleSite/content ./

salin default site config:

> cp themes/jane/exampleSite/config.toml ./

Sekarang tinggal liat blognya sebelumnya ketikan perintah dibawah ini pada terminal


>hugo server


Kalo udah buka browser kita dan buka url berikut `localhost:1313`

untuk hasil blognya akan seperti ini

 ![Screenshot blog hugo.](/static/image/2020/juni/hasilhugo.png)

  