---
title: "Materi Matematika Numerik - Persamaan Diferensial "
date: 2020-10-15T15:43:48+08:00
lastmod: 2020-10-15T15:43:48+08:00
draft: false
tags: ["Catatan","Matematika Numerik"]
categories: ["Mata Kuliah"]
author: fadil

# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
# comment: false
# toc: false
autoCollapseToc: true
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
#contentCopyright: '<a href="https://github.com/gohugoio/hugoBasicExample" rel="noopener" target="_blank">See origin</a>'
# reward: false
# mathjax: false
---

#### Persamaan Diferensial

`Persamaan diferensial` adalah  suatu  persamaan  yang  memuat  turunan terhadap  satu  atau  lebih  dari  variabel-variabel  bebas  (independent  variables). Bila  hanya  ada  satu  variabel  bebas  yang  diasumsikan,  maka  subyek  disebut `persamaan  diferensial  biasa`  (ordinary  differential  equation).  Contoh persamaan diferensial biasa sebagai berikut : 

![contoh persamaan diferensial](https://u.cubeupload.com/fdll14/1.png)

Jika pada persamaan diferensial ada dua atau lebih variabel bebas dan memuat turunan   parsial   maka   dinamakan   `persamaan   diferensial   parsial`   (partial differential equation). Sebagai contoh:

![Contoh persamaan diferensial parsial](https://u.cubeupload.com/fdll14/2.png)

Untuk selanjutnya tulisan ini hanya  membahas persamaan diferensial biasa.

#### Sejarah Persamaan Diferensial

`Persamaan diferensial` pertama kali eksis dengan penemuan kalkulus oleh `Newton` dan `Leibniz`. Pada bab 2 hasil karyanya tahun 1671 berjudul `"Methodus fluxionum et Serierum Infinitarum"`,Isaac Newton menuliskan 3 macam persamaan diferensial: 

![3 macam persamaan diferensial oleh Isaac Newton](https://u.cubeupload.com/fdll14/3.png)

`Jacob Bernoulli` mengusulkan persamaan diferensial Bernoulli tahun 1695.Hasilnya berupa persamaan diferensial biasa dalam bentuk :

![versi jacob bernoulli](https://u.cubeupload.com/fdll14/4.png)

dimana pada tahun berikutnya `Leibniz` mendapatkan penyelesaian dengan menyederhanakannya.

Secara historis, masalah senar bergetar seperti instrumen musik dipelajari oleh `Jean le Rond d'Alembert`, `Leonhard Euler`, `Daniel Bernoulli`, dan `Joseph-Louis Lagrange.` Tahun 1746, `d’Alembert` menemukan persamaan gelombang satu dimensi, dan 10 tahun kemudian `Euler` menemukan persamaan gelombang 3 dimensi.

Persamaan Euler–Lagrange dikembangkan tahun 1750-an oleh Euler dan Lagrange sehubungan dengan studi mereka mengenai masalah tautokron. masalah ini adalah menentukan kurva dimana partikel berbobot akan jatuh pada titik tertentu pada waktu tertentu, tidak tergantung dari titik awal. Lagrange menyelesaikan masalah ini tahun 1755 dan mengirim penyelesaiannya ke Euler. Keduanya kemudian mengembangkan metode Lagrange dan mengaplikasikannya ke mekanika, yang akhirnya membentuk perumusan mekanika Lagrangian.

Fourier mempublikasikan kerjanya mengenai aliran panas dalam Théorie analytique de la chaleur (Teori Analisis Panas), yang dimana didasarkan pemikirannya pada hukum pendinginan Newton, yaitu aliran panas antara 2 molekul berdekatan berbanding lurus dengan perbedaan temperatur. Termasuk di dalam buku ini adalah proposal Fourier mengenai persamaan panas untuk difusi panas konduktif. Persamaan diferensial parsial ini sekarang dipelajari oleh siswa fisika matematika.

##### Persamaan Diferensial Orde Satu 

`Persamaan Diferensial` orde satu adalah suatu fungsi yang memuat satu variabel bebas (x)  dan  satu  variabel  tak  bebas  (y)  beserta  turunan  pertamanya  (y′)  yang dikaitkan  secara eksplisit  atau implisit. `Solusi  umum  PD` adalah  fungsi yang  memuat  konstanta C  dan  memenuhi  PD  tersebut. ``Solusi  khusus` adalah  solusi  yang  diperoleh  dari  solusi  umum  dengan  mengambil  nilai C   suatu   bilangan   tertentu   atau   solusi   yang   memenuhi syarat   yang diberikan,  misalnya  syarat  awal.  Grafik  dari  solusi  umum  merupakan keluarga  lengkungan,  di  mana  untuk  setiap  nilai C  diperoleh  suatu lengkungan (kurva) atau trayektori. PD  yang  solusi  umumnya  diberikan  oleh  fungsi g(x,y,C)  = 0  dapat ditentukan dengan mengeliminasi C dari kedua persamaan: 

![contoh](https://u.cubeupload.com/fdll14/8.png)

dengan mengingat y sebagai fungsi dari x.

###### Definisi 1
Suatu PD orde satu dapat dinyatakan secara umum dalam dua bentuk, yaitu:

Bentuk implisit, 

![bentuk implisit](https://u.cubeupload.com/fdll14/5.png)

Bentuk eksplisit, 

![bentuk eksplisit](https://u.cubeupload.com/fdll14/6.png)

Contoh-contoh mengidentifikasi PD orde satu: 

![contoh-contoh PD](https://u.cubeupload.com/fdll14/7.png)

###### Definisi 2

Suatu fungsi  y = y(x)  dikatakan solusi PD  (1) atau (2) apabila   y = y(x) dan turunannya  y′ memenuhi PD (1) atau (2).  
![definisi dua](https://u.cubeupload.com/fdll14/definisi2.png)

##### Keluarga Lengkungan (Kurva) 

Anda  telah  mengetahui  solusi  umum  suatu  PD  memuat  konstanta C.  Jadi solusi umum dapat ditulis dalam bentuk y=y(x,C).

Solusi umum PD: cosyx′= adalah (  ,   )sinyy  x Cx    C==+.  Grafik   dari   solusi   umum (  ,   )yy  x C=   merupakan   keluarga   lengkungan (kurva) karena untuk setiap pengambilan nilai C diperoleh suatu lengkungan solusi khusus. 

![keluarga lengkungan](https://u.cubeupload.com/fdll14/contoh16.png)

![keluarga lengkungan](https://u.cubeupload.com/fdll14/9.png)

![keluarga lengkungan](https://u.cubeupload.com/fdll14/10.png)

##### Persamaan Diferensial homogen( homogenousD E )

PD orde satu dalam bentuk y'= f ( x,y ) dikatakan homogen jika, untuk sebarang bilangan real t,berlaku : f(tx,ty)=f(x,y)

###### Persamaan Diferensial terpisahkan( separable DE )

Bentuk umum: 

A(x) dx + B(y) dy = 0

Solusi: 

![solusi](https://u.cubeupload.com/fdll14/13.png)

#### Problem nilai batas
Jika PD disertai dengan kondisi dimana fungsi dan turunannya diberikan nilai pada variabel bebas yang berbeda, maka kondisi ini disebut problem nilai batas



#### Contoh soal

Contoh Soal Diferensial

![contoh soal 1](https://u.cubeupload.com/fdll14/12.png)

![contoh soal 2](https://u.cubeupload.com/fdll14/11.png)

#### Anggota Kelompok 3 Materi Persamaan Diferensial

1. Muhamad Shuro Fadhillah - 19090072
2. Rian pertama - 19090069
3. Agung Iswanto - 19090003
4. Gina Sonia Wiranti - 19090062
5. Novita Fitria Putri - 19090130
6. Dhimas Abiyasa - 19090108
7. Gilang maulana Alamsyah - 19090103

#### Sumber : 

[Wikipedia - Persamaan Diferensial](https://id.wikipedia.org/wiki/Persamaan_diferensial)

[Persamaan Diferensial Oleh Drs. Rochmad, M.Si](http://maulana.lecture.ub.ac.id/files/2014/09/persamaandifferensial.pdf)

[Rumusbilangan - Persamaan Diferensial](https://rumusbilangan.com/persamaan-diferensial/)

[Persamaan Diferensial Tingkat Satu](http://share.its.ac.id/mod/page/view.php?id=1740)

[Persamaan Diferensial - Binus](https://dinus.ac.id/repository/docs/ajar/1-Persamaan_Diferensial_Orde_Satu.pdf)


